"use strict";
// Задание 6
// Дан обьект employee. Добавьте в него свойства age и salary, не изменяя изначальный объект (должен быть создан новый объект, который будет включать все необходимые свойства). Выведите новосозданный объект в консоль.
const employee = {
  name: "Vitalii",
  surname: "Klichko",
};

const user = { ...employee, age: 2, salary: 0 };
console.log(user);
