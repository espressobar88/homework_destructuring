"use strict";
// Задание 7
// Дополните код так, чтоб он был рабочим
// alert(value);  должно быть выведено 'value'
// alert(showValue());   должно быть выведено 'showValue'

const array = ["value", () => "showValue"];

const [value, showValue] = array;
alert(value);
alert(showValue());
